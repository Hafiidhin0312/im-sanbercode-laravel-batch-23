<?php
require_once 'animal.php';
require_once 'Ape.php';
require_once 'Frog.php';



$sheep = new Animal("shaun");
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

echo "Name : ".$sheep->get_name()."\n"; 
echo "Legs : ".$sheep->get_legs()."\n"; 
echo "Cold_Blooded : ".$sheep->get_blooded()."\n"; 

echo "\n";

echo "Name : ".$kodok->get_name()."\n"; 
echo "Legs : ".$kodok->get_legs()."\n"; 
echo "Cold_Blooded : ".$kodok->get_blooded()."\n"; 
echo "Jump : ".$kodok->jump()."\n"; 

echo "\n";

echo "Name : ".$sungokong->get_name()."\n";
echo "Legs : ".$sungokong->get_legs()."\n"; 
echo "Cold_Blooded : ".$sungokong->get_blooded()."\n";
echo "Yell : ".$sungokong->yell()."\n"; 

?>