<?php
function ubah_huruf($string){


  $ubahkata="";

  for ($i=0; $i < strlen($string); $i++) { 
    $ascii = ord($string[$i]);
    $huruf_after = chr($ascii+1);
    $ubahkata.=$huruf_after;
  }

  return $ubahkata;
  
}

// TEST CASES
echo ubah_huruf('wow')."\n"; // xpx
echo ubah_huruf('developer')."\n"; // efwfmpqfs
echo ubah_huruf('laravel')."\n"; // mbsbwfm
echo ubah_huruf('keren')."\n"; // lfsfo
echo ubah_huruf('semangat')."\n"; // tfnbohbu


?>