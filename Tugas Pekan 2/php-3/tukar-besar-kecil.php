<?php
function tukar_besar_kecil($string){

  //cara 1
  
  $ubahkalimat="";
  
  //cara 1 - Menggunakan fungsi built-in ctype_upper

  for ($i=0; $i < strlen($string) ; $i++) { 
    if(ctype_upper($string[$i])){
      $ubahkalimat.=strtolower($string[$i]);
    }else{
      $ubahkalimat.=strtoupper($string[$i]);
    }
    
  }

  // cara 2 - menggunakan kode ASCII

  // for ($i=0; $i < strlen($string) ; $i++) { 
  //   if(ord($string[$i])>64 && ord($string[$i])<91){
  //     $berubah = ord($string[$i])+32;
  //     $ubahkalimat.=chr($berubah);
  //   }else if(ord($string[$i])>96 && ord($string[$i])<123){
  //     $berubah = ord($string[$i])-32;
  //     $ubahkalimat.=chr($berubah);
  //   }else{
  //     $ubahkalimat.=$string[$i];
  //   }
  // }

  return $ubahkalimat;

}

// TEST CASES
echo tukar_besar_kecil('HELLO wORLD')."\n"; // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY')."\n"; // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!')."\n"; // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me')."\n"; // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW')."\n"; // "001-a-3-5tRDyw"

?>