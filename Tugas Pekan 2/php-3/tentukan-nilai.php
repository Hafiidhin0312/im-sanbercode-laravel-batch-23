<?php

function tentukan_nilai($nilai){

  if($nilai>=85 && $nilai<=100){
    return "Sangat Baik";
  }else if($nilai>=70 && $nilai<85){
    return "baik";
  }else if($nilai>=60 && $nilai<70){
    return "Cukup";
  }else if($nilai>100 || $nilai<0){
    return "Nilai diluar Range";
  }else{
    return "Kurang";
  }

}


  //TEST CASES
  echo tentukan_nilai(98)."\n";
  echo tentukan_nilai(76)."\n"; 
  echo tentukan_nilai(67)."\n"; 
  echo tentukan_nilai(43); 

?>