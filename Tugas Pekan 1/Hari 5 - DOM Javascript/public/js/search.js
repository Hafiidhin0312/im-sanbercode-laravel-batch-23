import{list} from './items.js'
import{cart} from './cart.js'

export function search() { 
  //------------------  Section (search) -----------------
  var view = document.getElementById("listBarang");
  var items=list()
 

  var forminput = document.getElementById("formItem");

  forminput.addEventListener("submit", function (event) {
    event.preventDefault();

    var caribarang = document.getElementById("keyword").value.toLowerCase();
    var flag = false;
    var data_search = "";

    for (let i = 0; i < items.length; i++) {
      if (items[i][1].toLowerCase().search(caribarang) !== -1) {
        flag = true;

        data_search += `<div class ="col-4 mt-2">
<div class="card" style="width: 18rem;">
  <img src="${items[i][4]}" class="card-img-top" height="200px" width="200px" alt="...">
  <div class="card-body">
      <h5 class="card-title" id="itemName">${items[i][1]}</h5>
      <p class="card-text" id="itemDesc">${items[i][3]}</p>
      <p class="card-text">${items[i][2]}</p>
      <a href="#" class="btn btn-primary" id="addCart${i}">Tambahkan ke keranjang</a>
  </div>
</div>
</div>`;
      }
    }

    if (flag === true) {
      view.innerHTML = data_search;
    } else {
      view.innerHTML = `<div style="margin:0 auto;margin-top:200px;width:700px;text-align:center;font-size:30px">
        <i class="fas fa-exclamation-circle fa-3x" aria-hidden="true" style="color:red;"></i><p> Maaf Barang Tidak Ditemukan</p>
        </div>`;
    }

    cart() //panggil fungsi cart

  });

 }