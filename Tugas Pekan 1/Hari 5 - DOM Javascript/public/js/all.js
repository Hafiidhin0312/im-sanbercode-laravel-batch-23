import { cart } from './cart.js';
import{list} from './items.js'
export function all() { 


 //------------------ Dashboard Section (all) -----------------

var items=list()
 var view = document.getElementById("listBarang");

 var data_all = "";

 for (let i = 0; i < items.length; i++) {
   data_all += `<div class ="col-4 mt-2">
<div class="card" style="width: 18rem;">
 <img src="${items[i][4]}" class="card-img-top" height="200px" width="200px" alt="...">
 <div class="card-body">
     <h5 class="card-title" id="itemName">${items[i][1]}</h5>
     <p class="card-text" id="itemDesc">${items[i][3]}</p>
     <p class="card-text">${items[i][2]}</p>
     <a href="#" class="btn btn-primary" id="addCart${i}">Tambahkan ke keranjang</a>
 </div>
</div>
</div>`;
 }

 view.innerHTML = data_all;
 cart();


 }