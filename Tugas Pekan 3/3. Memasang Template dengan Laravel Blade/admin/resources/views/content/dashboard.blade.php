@extends('master')

@section('titles')
   <h2>Dashboard</h2>
@endsection

@section('content')


<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Tugas 3 Pekan 3 Templeting Blade Laravel</h3>

      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
    <div class="card-body">
      <p>
      Lorem ipsum dolor sit amet consectetur, 
      adipisicing elit. Amet magnam corrupti suscipit, 
      dignissimos architecto autem soluta nihil quas accusamus 
      omnis facere qui ex nostrum incidunt obcaecati vero iure in nisi.
    </p>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      &copy; Hafiidhin
    </div>
    <!-- /.card-footer-->
  </div>
  <!-- /.card -->

</section>
<!-- /.content -->

@endsection