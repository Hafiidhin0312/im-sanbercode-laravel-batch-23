@extends('master')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Show Cast {{$data->nama}}</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <h2>{{$data->nama}}</h2>
    <h3>{{$data->umur}} Tahun</h3>
    <p>{{$data->bio}}</p>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    Footer
  </div>
  <!-- /.card-footer-->
</div>
<!-- /.card -->   
@endsection