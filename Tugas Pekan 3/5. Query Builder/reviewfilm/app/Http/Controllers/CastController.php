<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){

      $casts = DB::table('casts')->get();
      return view('content.index', compact('casts'));
      
    }

    public function create(){

      return view('content.create');
      
    }

    public function store(Request $request){
      $request->validate([
        'nama' => 'required|unique:casts',
        'umur' => 'required',
        'bio' => 'required',
        
    ]);
    $query = DB::table('casts')->insert([
        'nama' => $request["nama"],
        'umur' => $request["umur"],
        'bio' => $request["bio"]
    ]);
    return redirect('/cast');
    }

    public function show($id){
      $data = DB::table('casts')->where('id', $id)->first();
      return view('content.show', compact('data'));
    }

    public function edit($id){
      $data = DB::table('casts')->where('id', $id)->first();
   
            return view('content.edit', compact('data'));

            
    }

    public function update($id, Request $request){
      $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
        
    ]);

    $query = DB::table('casts')
        ->where('id', $id)
        ->update([
          'nama' => $request["nama"],
          'umur' => $request["umur"],
          'bio' => $request["bio"]
        ]);
    return redirect('/cast');
    }

    public function destroy($id){
      $query = DB::table('casts')->where('id', $id)->delete();
      return redirect('/cast');
    }
}
