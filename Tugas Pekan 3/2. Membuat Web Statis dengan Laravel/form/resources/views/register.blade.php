<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Form</title>
</head>

<body>
  <h1>Buat Account Baru !</h1>

  <h3>Sign Up Form</h3>
  <form action="{{url('/welcome')}}" method="POST">
     @csrf
    <label for="fname">First name:</label><br /><br />
    <input type="text" id="fname" name="fname" /><br /><br />
    <label for="lname">Last name:</label><br /><br />
    <input type="text" id="lname" name="lname" /><br /><br />

    <label for="gender">Gender:</label><br /><br />

    <input type="radio" id="male" name="gender" value="male" />
    <label for="male">Male</label><br />
    <input type="radio" id="female" name="gender" value="female" />
    <label for="female">Female</label><br />
    <input type="radio" id="other" name="gender" value="other" />
    <label for="other">Other</label>
    <br /><br />

    <label for="nationality">Nationality :</label><br /><br />
    <select name="nationality">
      <option value="indonesia">Indonesia</option>
      <option value="malaysia">Malaysia</option>
      <option value="singapura">Singapura</option>
      <option value="australia">Australia</option>
    </select>

    <br /><br />
    <label for="language">Language Spoken :</label> <br /><br />
    <input type="checkbox" id="id" name="id" value="Bahasa indonesia" /><label for="id">Bahasa Indonesia</label><br />
    <input type="checkbox" id="en" name="en" value="English" /><label for="en">English</label><br />
    <input type="checkbox" id="ot" name="ot" value="Other" /><label for="ot">Other</label><br />

    <br /><br />
    <label for="bio">Bio:</label><br /><br />

    <textarea id="bio" name="txtarea" rows="9" cols="30"> </textarea>

    <br />
    <button type="submit">Sign Up</button>
  </form>

</body>

</html>