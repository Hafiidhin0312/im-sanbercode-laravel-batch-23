<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CastModel extends Model
{
    protected $table = "casts";
    protected $fillable = ["nama", "umur","bio"];
    public $timestamps = false; //butuh ini jika di tabel tidak ada kolom timestamps
}
