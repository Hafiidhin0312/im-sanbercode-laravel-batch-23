<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CastModel;
use DB;

class CastController extends Controller
{
    public function index(){

      $casts = CastModel::all();
      return view('content.index', compact('casts'));
      
    }

    public function create(){

      return view('content.create');
      
    }

    public function store(Request $request){
      $request->validate([
        'nama' => 'required|unique:casts',
        'umur' => 'required',
        'bio' => 'required',
        
    ]);
        CastModel::create([

          'nama'=>$request->nama,
          'umur'=>$request->umur,
          'bio'=>$request->bio

        ]);
 
    return redirect('/cast');
    }

    public function show($id){
      $data = CastModel::find($id);
      return view('content.show', compact('data'));
    }

    public function edit($id){
      $data = CastModel::find($id);
      return view('content.edit', compact('data'));
    }

    public function update($id, Request $request){
      $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
        
    ]);

    $data  = CastModel::find($id);
    $data->nama = $request->nama;
    $data->umur = $request->umur;
    $data->bio = $request->bio;

    $data->update();

    return redirect('/cast');
    }

    public function destroy($id){
      $data  = CastModel::find($id);
      $data->delete();
      return redirect('/cast');
    }
}
