
@extends('master')

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Tambah Cast</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    <div class="container">
      <form action="{{url('/cast')}}" method="POST">
        @csrf
        <div class="form-group">
          <label for="email">Nama :</label>
          <input type="text" class="form-control" placeholder="Nama Cast" id="email" name="nama">
          @error('nama')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group">
          <label for="pwd">Umur :</label>
          <input type="text" class="form-control" placeholder="Umur" id="pwd" name="umur">
          @error('umur')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        <div class="form-group">
          <label for="comment">Bio:</label>
          <textarea class="form-control" rows="5" id="comment" name="bio"></textarea>
          @error('bio')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
      @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>      
    </div>
    
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    Footer
  </div>
  <!-- /.card-footer-->
</div>
<!-- /.card -->   
@endsection



